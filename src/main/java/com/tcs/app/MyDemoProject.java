package com.tcs.app;

public class MyDemoProject {

    private static final int AGE_MIN = 18;

    public static void main(String[] args) {

        System.out.println("Demo para version 1.9.0");

        int age = Integer.parseInt(args[0]);

        boolean isAgeMin;

        isAgeMin = age < AGE_MIN;

        if (isAgeMin){
            System.out.println(new DemoApp().getGreeting());
        }

    }


}
