package com.tcs.app;

import org.junit.Test;

import static org.junit.Assert.*;

public class DemoAppTest {

    @Test public void testAppHasAGreeting() {
        DemoApp classUnderTest = new DemoApp();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}